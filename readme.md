# OS

Stores configurations, some notes, and other resources for a few OS installs.
This used to have a few notes about research on operating systems, package
management, containers, compilers, etc but I've migrated that to a TiddlyWiki
instead.
