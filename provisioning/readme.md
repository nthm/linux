# Bash is okay

This a provision script to help setup a VM. Written assuming it'll run on a
generic Linux box with systemd. Actually a CentOS 7 box, so unfortunately
portability is limited.

I'm not thrilled but it works well. Ideally a script would not be written in
bash, and would instead be run remotely from a more competent machine. There
should be commits+caching like Dockerfiles do, or at least some state file that
tracks the changes which have been done on previous runs. Work can be expensive,
so it's good to skip things.

Ansible does that, but it's a lot to learn and not that lovely with its 750
modules and being in Python. SSH-based is good though. I don’t want an agent or
need to install Node/TS on the system. Just let me run remotely.

This kind of complexity isn't great for bash, and you'd want to have objects and
data structures to managing this information. For instance, directories for
downloads (with their hashes), templates, files, and then all the metadata about
the work performed along with the stdout/stderr logs. If it could be recorded in
a script-replay like style that'd be great.

If you have a competent system running Python or Node you can SSHFS the remote
server into your local and chroot into it.

I'd like it to step through commands either one by one or in a unit of work that
can be undone or cleaned up if it fails. I understand that sandboxing is
generally the solution to this but not for a host or where direct access is
literally the goal.

## Bash commons

It's a lot of

```sh
set -o xtrace
set -e

cat << 'EOT' > /path/to/file
File content
EOT

echo
echo

cd
run-things
```

With random commands and a super imperative directory changes. Whoops.

## Ideas for something more ideal

```ts
import { log, sh, oneLine, stripIndent, fsRemote } from "@local/common-utils";

// Ensure that all file operations are actually happening inside the remote
fsRemote.setRoot(path.resolve('~/_/sshfs-servername/'))

const stepConfigureSSH = {
  async run() {
    const exists =
      (await sh`pakku -Qs ssh`).exitCode === 0
    const running =
      (await sh`systemctl status sshd`).match(/Active: active \(running\)/)

    // Or if you wanted to be robust check for a .pacsave or systemd unit file...
    // For instance, some distributions use /etc/ssh/sshd_config
    await fsRemote.editFile('/etc/sshd/config', content => {
      content = content.replace(/(PasswordAuthentication).*No/i, '$1 Yes')
      content += stripIndent`
        # Some additional content
        UseDNS No
      `
    })
    if (running) await sh`systemctl restart sshd`
    log(await sh`sudo ss -nlptu`)
  },
  async cleanup() {
    // If an exception is thrown?
  }
}

// Or maybe not even a step-runner-based system...
const traefikConfig = await fsRemote.readFile('/etc/traefik.toml')
traefikConfig.replace(/(?<groupName>A|B)/, () => {
  // ...
})

// Likely in another main.ts file
const easyRuns = [stepConfigureSSH]
const preventRuns = [stepExpensiveFileDownload, stepLongFSCK, stepSSDTrim]

function stepRunner() {
  try {
    await step.run()
  } catch () {
    if (step.cleanup) {
      // ...
    }
    // If it doesn't break dependencies do we continue? Message someone?
  }
}
```

You can see how this very quickly enters the realm of topologically sorted
dependency management, fallbacks, interactive patchwork (think REPL where you
step in and make changes to a failed job before continuing other work)

It does work, though. It's about managing the complexity of the system and
having it not become a 1000 line JS blob - you want as many concept as possible
to be modules. It would need a lot of planning.

Hey wait. This is redoing Jenkins in JS. There's nothing wrong with that, but
that's where this came from after I've been doing Jenkinsfiles for too long.
