function fish_prompt --description 'Write out the prompt'
    # Right-align the clock
    # printf "%*s\r%s\n" $COLUMNS (date '+%I.%M.%S')" " " "(prompt_pwd)

    # Save the return status of the previous command
    set stat $status

    echo -n (date "+%I.%M.%S") ""

    set_color -o blue
    if not test $fish_prompt_pwd_dir_length
        set -U fish_prompt_pwd_dir_length 3
    end

    echo -n (prompt_pwd)

    if test $stat -gt 0
        set_color -o red
        echo -n "" \($stat\)
    end

    set_color -o normal
    echo \n
end
