# Lychee

Hosted a cloud and some services from 2018-2020.

It was a Parabola Linux install on a Dell desktop. Mirrored 1TB and 2TB drives
with BTRFS and full disk encryption that unlocks with a USB key.

Infrastructure was on Docker (considered Kube but realized its overengineered
and overhyped). Host services were PostgresSQL and ones that used the WireGuard
tunnel via netns such as rTorrent (which is why I can't use CoreOS/Rancher/etc).

Moved more and more towards less stressful updates. Want a minimal host for
containers. Used to have an full XFCE4 setup but removed 1.5GB of packages to
lighten up.

Hosted at home. I used to use a WireGuard tunnel to a VPS with a static IP in
Montreal but the RTT was huge. Now I use dynamic DNS with 1984.is. Plus they do
DNSSEC which is lovely.

Traefik ties the infrastructure together and manages HTTPS certificates. Docker
containers are all managed through Portainer. The Compose files in this repo are
for Portainer stacks so they're written as v2.

## Running

Git, Nextcloud, Gotify, Wordpress, Bitwarden, Portainer, Traefik(+LE), VSCode
server, project hosting.

## In progress

I'd like to have VSCode on the host instead of a container. I'd need stronger
auth for that. Similarly I wanted to host TiddlyWiki notebooks but I don't have
good auth and metrics/analytics yet.

Considered Authelia for auth but decided Ory.sh SSO with OAuth would be ideal.
Just have to do it. Metrics are harder to do. Gotify is a transport but honestly
I don't have logging, alerts, triggers of any kind... It's too open right now.

## Future / Nice to haves

Pleroma social or Mastedon. Telegram bot of some kind. TiddlyWiki 5 hosting (I'd
need auth first). Firefly finance accounting app. Flood torrenting app.

## System notes

- [ ] Network namespace some systemd services to enforce WireGuard
- [ ] Safely auto-update + restart/kexec the OS using auto-rollbacks via Btrfs
- [x] Dynamic DNS IP check using a systemd timer
- [ ] Encrypted offsite full system backups to another server
- [ ] System-wide file management and SSH access both through a web browser ⚠️
- [ ] Notifications of disk use, network traffic, etc (see Gotify)

In hindsight moving _away_ from systemd is a goal of mine. I'd love to use runit
and super basic cron-like apps instead.

## Researched

Switching to an encrypted Nextcloud: Not worth the overhead and risk of
encryption messing up and losing content - it's better for my friends to host
their own server and federate with me. Zero trust.

Sending email: This is...large. Postfix/DKIM/DARCS/SPF/DNS TXTs/IMAP/POP...
Possible. Probably a container. Too difficult right now.
