# Dynamic DNS via 1984.is API

Lil dynamic DNS helper service/timer/script

_/etc/systemd/system/dynamic-dns.service_
```
[Unit]
Description=Dynamic DNS renewal
After=network.target

[Service]
ExecStart=/etc/systemd/system/dynamic-dns/check

[Install]
WantedBy=basic.target
```

_/etc/systemd/system/dynamic-dns.timer_
```
[Unit]
Description=Run 1984 dynamic DNS every day

[Timer]
OnCalendar=*-*-* 4:00:00
Persistent=true

[Install]
WantedBy=timers.target
```

_/~/bin/dynamic-dns-check_
```
#!/bin/bash
set -e

API_KEY=">.>"
DOMAIN=">.>"

FILE=/var/run/dynamic-dns-latest
if [ ! -e $FILE ]; then
    echo "Creating $FILE"
    touch $FILE
fi

LATEST=$(cat $FILE)
NEW=$(curl -s ipecho.net/plain)

# If it's OK then leave
if [ "$LATEST" == "$NEW" ]; then
    echo "IP hasn't changed from $LATEST"
    exit 0
fi

echo "Updating from $LATEST to $NEW"
echo $NEW > $FILE
# Don't pass in the $NEW IP, let the service determine it
curl -v "https://api.1984.is/1.0/freedns/?apikey=$API_KEY&domain=$DOMAIN&ip="
```

Check permissions on `current` file too
