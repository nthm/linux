# Dracut initramfs with easy remote LUKS decryption via OpenSSH

> TODO: This will be updated when my kernel is done

> TODO: Look into https://docs.voidlinux.org/config/security/hashboot.html for
> the initramfs

Unlocking via SSH at boot shouldn't be very hard, but many articles and wiki
pages will describe it as a painful thing to do and require TinySSH or Dropbear.
These SSH daemons are probably fine for some, but use their own host key format,
may not support your SSH public key (or only support ED25519 keys) and usually
want to be installed on a non-standard port.

That sucks. You don't want to have to remeber to pass a special public key for
this specific situations, ~~or have the connection refused for an incorrect host
key where _"Someone's possibly doing something nasty!"_.~~ Actually this is an
unfortunate necessary evil because host keys shouldn't be in the unencrypted
disk image.

## Merging Arch's mkinitcpio with systemd

I tried `mkinitcpio-systemd-tool` ([GitHub][1]) which seemed very promising but
after reading the code seemed largely undocumented and poorly written. Sorry,
but it's a very awkward wiring of shell scripts to systemd services, and while
the idea of having systemd _shared_ between initramfs and the real system is
neat ([and possible][2]) I don't know if it's actually a good thing. Having the
initramfs as a container of sorts has benefits of seperations of concerns and
complexity. I experienced sketchy systemd startup hangs and consolefont flickers
as it was clear some of the work was being done twice.

## Custom initramfs' are doable (distri+go)

Having read a wonderful post on writing an [distri's initramfs from scratch][3]
in (mostly) Golang, and from poking around a lot of the mkinitcpio scripts used
from `/usr/lib/initcpio/`, I was convinced for a bit that writing one from
scratch might just need to happen - want something simple done right then do it
yourself. This is an awful idea (I know I know) and thankfully the end of the
distri post convinced me to checkout dracut - for now.

## Dracut

Despite worries that dracut would be an overengineered monolith, it ended up
being a nice modularized collection of shell scripts with a systemd base. When
building in `--hostonly` mode, the image is reduced to only what you need (not
sure how it walks the module tree; there's likely unneeded kernel modules but
it's reasonable).

An in depth write up comparing initramfs' SSH daemons is on [dracut-sshd][4],
which I appreciate a lot. The repo includes configuration for bundling the
OpenSSH daemon into dracut with a neglible change to image size.

I had to make a bunch of changes to the repo such as adding a "privilege
seperation directory" at /var/empty and changing the sshd.service `Type=`. The
setup is largely OS independent. It still depends on systemd, though. It seems
dracut can work with busybox and not require systemd, but this would be a larger
undertaking and I'd rather just build ontop of Alpine (see next secton).

I tried using dhcpcd in 2dbe8ba to replace systemd-networkd, but it conflicts
with boot order of services, doesn't start, or had issues about binding to an
interface that systemd-udevd later (race condition) tries to rename from eth0 to
enp4s0 (host specific naming).

To install copy the three files to a folder under `46sshd` (which is named to
fit in the standard levels of priority for dracut)

```
sudo cp -ri 46sshd /usr/lib/dracut/modules.d
# Link or copy authorized keys over
sudo ln -s /home/you/.ssh /root/.ssh
sudo dracut -f -v
```

**Final image was only 15M**

The [dracut-sshd][4] repo as a section in their readme about host key based
attack vectors that's worth a read. You shouldn't store your host key in an
unencrypted initramfs since someone can copy your server identity.

I used to use $SUDO_USER so `ssh you@stayknit.ca` would work for initrd, but
that's being dishonest - during that stage of boot only UID 0 exists.

```
Host stayknit
    Hostname stayknit.ca
    User you

Host stayknit-unlock
    Hostname stayknit.ca
    # I used to use $SUDO_USER /etc/passwd alias but let's be honest...
    User root
    # Avoid SSH host key conflict
    HostKeyAlias stayknit.ca-initrd
```

Finally, you can inspect the initramfs by unpacking it to a directory:

```
# Easier as root, depending where you're unpacking to
sudo su
cp /boot/initramfs-5.5.13-gnu-1.img /tmp/initramfs-5.5.13-gnu-1.img.gz
mkdir /tmp/initramfs/
cd /tmp
gzip -d initramfs-5.5.13-gnu-1.img.gz
cd /initramfs
cpio -i < ../initramfs-5.5.13-gnu-1.img
# List the initramfs
ll
```

## Future: Dropping systemd

I know I'm going to switch to a non-systemd distro. I've had it with systemd
constantly breaking things for the sake of it - It consumes so many components
and is always in a state of heavy development as a result. It's largely
over-engineering and I don't need it. I'm tired; of updates locking me out of
remote systems; of the journal rotating and losing all my logs; of having logs
spammed with "Condition Not Met" warnings; of the journal not being able to
delete on a per-service basis; of systemd desiring so strongly to be different
and to constantly be rewritten - only for the sake of being rewritten. All the
naming is different, units/devices/mounts/slices/targets okkk.

It's like they're trying to justify working in this field instead of moving on
and developing something higher in the tech stack. System management is
important but it's also finite. The work is done. People have init systems in
a few hundred lines of code that never need updating. It's done. Instead of
millions of lines that never finish developing.

I want to have a small reliable foundation that is fully understood, and then
move on with my life, onto other higher-level things.

## Future: Alpine and mkinitfs

I'll very likely end up building an Alpine distro, with runit. For now, it's
only the initramfs that matters, called [alpine/mkinitfs][5]. It has settled,
as a few files of shell and C. You're expected to hack on it directly and
change the Makefile.

It's mostly busybox. It uses lddtree to collect shared libraries for binaries
pulled in (via a feature-based module system); which is something dracut
probably does under the hood. Has some neat features like being able to boot a
diskless full Alpine installation into RAM from the internet (self-bootstrap)
and supporting [NDB (network block devices)][6] as well as LVM/MDADM/ZFS.

The source is easy to read and understand...except [nlplug-findfs.c][8], which
is a bit elusive at 1500 lines of C. It implements all the barebones of
managing threads and processes in linked lists in order to support spawning
LUKS decrypt, MDADM, and ZFS. The LUKS doesn't even use cryptsetup though; it
reaches out to [libcryptsetup][9] C library directly and implements its own
password-reading code that sets/restores TTY parameters... it's a lot. Lastly,
the program listens to udev events and responds accordingly to trigger the
above actions. I really don't think it needs to be this way, and would prefer
even all of bash/busybox/cryptsetup/dropbear to have a functioning emergency
shell instead of a complex homemade binary. It also misses features like more
than one crypt device (such as swap) and logging.

I'll look into when the time comes.

For completeness, I also tracked down [Debian's cryptsetup-unlock source][7]
since it's referenced a lot when Ubuntu/Debian initramfs' are discussed. Under
the hood it's still the cryptsetup binary, though.

## Today

I need to move on for now so I'll use systemd and systemd-networkd in the
initramfs because it _works_ (!). The only reason I tried dhcpcd was to reduce
the logging about Docker containers gaining carriers/leases. It, however, has
a ton of other issues like hanging the system for 1m30s at shutdown (?) and
having an error loop about IPv6 which might be kernel-specific (TODO).

[1]: https://github.com/random-archer/mkinitcpio-systemd-tool
[2]: https://www.freedesktop.org/wiki/Software/systemd/InitrdInterface/
[3]: https://michael.stapelberg.ch/posts/2020-01-21-initramfs-from-scratch-golang/
[4]: https://github.com/gsauthof/dracut-sshd

[5]: https://gitlab.alpinelinux.org/alpine/mkinitfs
[6]: https://nbd.sourceforge.io/
[7]: https://salsa.debian.org/cryptsetup-team/cryptsetup/-/blob/master/debian/initramfs/cryptroot-unlock

[8]: https://gitlab.alpinelinux.org/alpine/mkinitfs/-/blob/master/nlplug-findfs.c
[9]: https://gitlab.com/cryptsetup/cryptsetup/-/blob/master/lib/libcryptsetup.h
