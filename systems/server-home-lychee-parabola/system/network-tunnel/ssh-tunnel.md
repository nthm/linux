# Tunneling with SSH

I have a VPS with OVH; they're great. I use it as a reverse proxy where I can
open ports on a static IP address ~~, and a place where I can store encrypted
backups.~~

I used to push an SSH and reverse proxy port to bind to, but ended up preferring
a Wireguard tunnel instead once I learned [SSH tunnels can't forward the client
IP address][1]. This means all connections are from `::1`, making _sshguard_, IP
routing, and metric collection useless.

[1]: https://unix.stackexchange.com/q/427758/get-remote-ip-address-over-a-ssh-remote-port-forwarding-tunnel

I recommend using a real tunnel that creates an interface instead. I use
WireGuard because I want the tunnel encrypted, but there are other options too.

Here are some links for WireGuard:

- https://golb.hplar.ch/2018/10/wireguard-on-amazon-lightsail.html
- https://blog.rootshell.be/2009/03/01/keep-an-eye-on-ssh-forwarding/

If you need an SSH tunnel though, read on. They're great for small operations.

## SSH tunnel

Here's how to publish ports on the proxy. It'll need `GatewayPorts=` on in its
SSHD config.

```
proxy-tunnel.service

[Unit]
Description=SSH tunnel to reverse proxy
After=network.target
Wants=network.target
OnFailure=proxy-tunnel-retry.service

[Service]
User=<USER>
ExecStart=ssh -N -o ServerAliveInterval=60 -o ExitOnForwardFailure=yes -R 1022:localhost:22 user@<PROXY> -p 1000
Restart=on-failure
# Wait 10s between ssh invocations
RestartSec=10s
StartLimitBurst=3
# Network might hang and not fail immediately, so 3 failures could take 60s
StartLimitInterval=60s

[Install]
WantedBy=multi-user.target
```

```
proxy-tunnel-retry.service

[Unit]
Description=Retry tunnel connection

[Service]
TimeoutStartSec=infinity
# This has to be greater than the StartLimitInterval since systemd ignores calls
# to `systemctl start/restart` if it's within that window
ExecStartPre=/bin/sleep 10m
ExecStart=systemctl restart proxy-tunnel

[Install]
WantedBy=multi-user.target
```

This connects to the proxy on its SSH port (1000 in this example) and opens up
its port 1022 to point to the home server's port 22.

The idea is that the proxy is untrusted and doesn't have any credentials to
connect to the home server.

When there's a disconnection, systemd retries. If the restart policy caps out,
The `OnFailure=` is activated to try again later.

## Notice about IP blocking

All traffic for whatever ports you open will appear to come through ::1, so be
careful you don't ban yourself.

I was banned in SSHGuard until I whitelisted ::1 (which, is a bad workaround)

```
/etc/sshguard.conf

LOGREADER="LANG=C /usr/bin/journalctl -afb -p info -n1 -t sshd -o cat"
WHITELIST_FILE=/var/db/sshguard/whitelist
BLACKLIST_FILE=120:/var/db/sshguard/blacklist
BACKEND="/usr/lib/sshguard/sshg-fw-nft-sets"
```

With

```
/var/db/sshguard/whitelist

::1
```

---

## Previous attempts

I originally tried to get this behavious with a systemd timer, but that also
requires a service alongside the original service. It's messy.

I then tried to use `Before` and `Requires` which has been reported to work for
some people (?):

```
[Unit]
Description=Retry X service
Before=x.service
Requires=x.service

[Service]
ExecStart=/bin/sleep 30

[Install]
WantedBy=multi-user.target
```

Which tells systemd to execute the retry service _before_ executing `x.service`.
For some reason it doesn't work, and calls immediately instead of after the
sleep. Note there was mention of using `Wants` instead of `Requires` but both
didn't work on v242
