# Notes

Here's some notes about Nextcloud.

### Database conversion

Initially the Docker version will use SQLite (beautiful) but to convert data to
Postgres use:

```
docker exec -u www-data -it nextcloud /var/www/html/occ db:convert-type --all-apps pgsql nextcloud 172.17.0.1 nextcloud
```

Note I had, at the time, added the container to the bridge network and
configured Postgres to listen on all interfaces so it can be reached by 172 IP
range. Nextcloud updates its own `config.php`.

Today I'm using a socket that's bind mounted. Postgres doesn't even listen on a
port, which I'd recommend.

### Auto-upload on Android

This _doesn't_ upload existing items in the directory. If upload (even manual)
stops silently when in the background or when the device is off, the upload
queue is too large. Luckily this shouldn't happen unless its an initial upload.

There are issues on GitHub but the behaviour isn't reproducible.

### Previews

Thumbnails are based on the _file ID_ but documentation is lacking on how it
works, whether it uses a perceptual hash, and why they're sometimes wrong.
Updating an image of the same location/name doesn't update the thumbnail.

There aren't `occ` commands regarding previews, but thumbnails under appdata are
safe to delete along with their folders corresponding to their file IDs.

Also I wouldn't recommend pre-generation. Yes, on the fly is slower, but
thumbnails are huge.

### Adding files manually without a sync client

Files in the cloud are owned by `www-data` (which is `http` on my host) so to
write to it you'll need to fix permissions and then switch them back.

Here's adding music:

```
sudo chown you:you /var/www/nextcloud/you/files/Music
cp --reflink=always -v -r ~/Music /var/www/nextcloud/you/files/Music
docker exec -u www-data -it nextcloud /var/www/html/occ files:scan -v --path="/you/files/Music"
sudo chown http:http -R /var/www/nextcloud/you/files/Music/
```

Reflink copies are instantaneous in Btrfs. Here's also an option using Beets:

```
beet move -e -c -d /var/www/nextcloud/you/files/Music/
```

### Desktop client says _"HTTP transmission error ... directory list missing"_

I don't remember the exact message but if it fails to sync _after_ doing a file
ownership transfer then check "Shares" > "Shared with/to me" and look for weird
entries in the list. Unshare it.
