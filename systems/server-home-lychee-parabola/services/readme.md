# Compose

This defines a lot of the startup work for containers on Lychee. It's not
perfectly reproducible, and is missing a lot of information about configurations
and what data needs to be where. I wanted to use Kubernetes to jot all of that
down on paper but it's so much work and incredibly overengineering. I've read
for days and made little progress. Most people advise to stay away from it.

Provisioning using a homerolled system is probably best since Kube doesn't cover
all the bases I need anyway; the host, storage, and backups for example. I love
chasing the idea of reproducible builds and stateless systems but it's not
practical.

Here's at least something. Docker Compose has many hiccups (not to mention DNS
issues oof) but it does at least deploy containers. Documentation like this will
have to fill in the blanks for now.

## Stacks vs CLI

I used to use Portainer Stacks but found them quite difficult to use effectviely
since they're not available outside of the web UI. I need a CLI. Ideally support
for V3+ of docker-compose.

## Bring up all services

```
> npm -g i merge-yaml-cli
/home/yesterday/_/n/bin/merge-yaml -> /home/yesterday/_/n/lib/node_modules/merge-yaml-cli/cli.js
+ merge-yaml-cli@1.1.2
added 88 packages from 38 contributors in 5.030s

> merge-yaml -i **/docker-compose.yml -o all.yml
Merging files:
bitwarden/docker-compose.yml
nextcloud/docker-compose.yml
portainer/docker-compose.yml
traefik/docker-compose.yml
whoami/docker-compose.yml

Writing:
all.yml

> cat all.yml | docker-compose -f - up -d
```
