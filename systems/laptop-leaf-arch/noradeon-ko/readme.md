# How to

You'll need `linux-headers`.

Clone the repo, copy all files to `/usr/src/noradeon-0.1/` and then from
anywhere run: `sudo dkms install -m noradeon/0.1`.

```
Creating symlink /var/lib/dkms/noradeon/0.1/source ->
                 /usr/src/noradeon-0.1

DKMS: add completed.

Kernel preparation unnecessary for this kernel.  Skipping...

Building module:
cleaning build area...
make -j4 KERNELRELEASE=4.14.78-1-MANJARO -C /usr/lib/modules/4.14.78-1-MANJARO/build M=/var/lib/dkms/noradeon/0.1/build....
cleaning build area...
Kernel cleanup unnecessary for this kernel.  Skipping...

DKMS: build completed.

noradeon.ko:
Running module version sanity check.
 - Original module
   - No original module exists within this kernel
 - Installation
   - Installing to /usr/lib/modules/4.14.78-1-MANJARO/kernel/drivers/platform/x86/

depmod.....

DKMS: install completed.
```

That's all.
