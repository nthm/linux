Linux on Envy 14 1154

The model from 2010 has only two issues worth mentioning as of 2015

#1 Btrfs superblock corruption:

Arch with btrfs came with an fstrim service, but each time it ran (every few
reboots) it overwrotes an important area at the start of the partition and
the next reboot hung at the bios with 'No OS'. The solution was to pass an
offset argument to the fstrim service

#2 Graphics card conflicts:

The laptop's two graphics cards were suspected to be an issue for Linux before
it was installed, and while the Arch kernel worked out of the box, there were
conflicts with snd_hda_intel (audio driver) and the radeon audio driver

Also systemd tried restoring backlight preferences of four video cards:
radeon_bl0, acpi_video0, acpi_video1, and intel_backlight which are under
/sys/class/backlight/

When adjusting the brightness, it was clear each card tried to make the change
and each with a different amount - which is a mess

The system could sleep and resume without issue and dmesg showed LNXVIDEO:00
and :01 both restoring backlight states. Issues began when snd_hda_intel hit a
'CORB reset timeout#2, CORBRP 65535' when launching lspci, an X server, or
trying to either shutdown or reboot the computer. The system would hang on
shutdown until the watchdog took over after 10 minutes. Forums described
similar issues with systems that has two sound cards. Sometimes the system
didn't freeze immediately, but would become more unstable as time went on and
more errors would come up such as: 'INFO: task kworker blocked for more than
120 seconds.' until the system froze

https://bbs.archlinux.org/viewtopic.php?id=196996 suggested writing 'options
snd-hda-intel enable=0,1,0' to /etc/modprobe.d/ which didn't work. Neither did
blacklisting radeon at either modprobe.d or the bootloader in an attempt to
disable the dedicated graphics card. Forcefully removing the module (rmmod -f)
would crash the system from dependencies failing after radeon

VGA Swicheroo was an option and actually worked, but had issues when coming
out of sleep. Researching how to completely disable the card lead to a project
called acpi_call https://github.com/mkottman/acpi_call which can be set up as
a systemd one shot unit on boot. It worked most of the time and the card is
seen turning off in dmesg along with a kernel oops. However, sometimes the
kernel panics and crashes due to the card's shutdown

Compiling a kernel without the radeon module (in hopes of not initializing the
card) didn't work - even without module support and no initramfs it finds a
way to load somehow. Wasn't interested in pursuing it because it didn't seem
promising

The ACPI table wiki pages said the firmware for the card could be removed from
the DSDT and SSDT but notes that modifying the tables is dangerous and
unsupported. ACPI DSDT/SSDT editing is popular with hackintosh computers and a
forum had a guide for disabling the dedicated card for an HP laptop by sending
the _OFF() function inside the _INI() function of the ACPI table - looked
promising and allows any kernel to be used, but wasn't exactly the case

Guide here: Used this for everything. Skipped renaming GFX0 to IGPU.
http://attabot.org/mac-os/turn-off-discrete-amd-radeon-graphics-hackintosh-ssdt/

This lead to two options:

1) Replace the SSDT by patching the kernel with code made by an Intel developer
in 2004 as a bug fix. The patch files went beyond replacing an SSDT and made
sure the OS dropped all other SSDTs once one is replaced. The bug fix was
closed as WILL_NOT_FIX after people mentioned problems with the DSDT requiring
SSDTs that had their methods changed or even removed. The bug's thread decided
it was best to compress all SSDTs into one DSDT and override that file using a
method already provided as a kernel compilation option. This laptop has six
SSDTs and learning another language to merge tables is a lot of work

https://bugzilla.kernel.org/show_bug.cgi?id=3774

2) ACPI Custom Method Overriding: Allows ACPI methods to be overriden (or
created if they don't exist) in user space during runtime. This sounded great
but had to be called from userspace as a systemd unit which caused similar
issues to acpi_call where the card reinitialized when coming out of sleep. The
source code showed how it was possible to execute in kernel space: Normally, a
kernel flag creates a file in debugfs on boot that allows .aml files to be
sent to it. The AML is put in a buffer and acpi_install_method(buffer) is
called, tainting the kernel

The Unix & Linux StackExchange showed that `acpi_backlight=vendor` as a boot
parameter removed acpi_video0 in /sys/class/backlight. Then calling acpi_call
turned off the card with no kernel oops. Suspending the computer didn't
reactivate it. Calling ACPI custom_method (from userspace) could make sure the
card stayed off if it was somehow turned on but it's concerning that the
system starts up using the card and then it's pulled away. The backlight was
not handled by the card but it was unclear is if audio was

Kernel space solution:

Found a GitHub which containing an ACPI fix for a Clevo laptop battery bug.
They patched the battery though a .dsl file and pasted the compiled AML code
as an array in a kernel module. They then passed the array to
acpi_install_method(array). Wrote the INI -> OFF AML code as a similar module
and tried it out

However, the module loaded too late. This was the dmesg for ACPI: 
[    0.000041] ACPI: Core revision 20150410
[    0.017157] ACPI: All ACPI Table successfully acquired
[    0.288745] ACPI FADT declares the system doesn't support PCIe ASPM
[    0.288749] ACPI: bus type PCI registered
...
[    0.317019] ACPI : EC: EC started
[    1.091687] ACPI: Interpreter enabled

Where tables are parsed and executed (see acpi/bus.c)

PCI went on to setup all devices using rules found in ACPI tables. There was
lots of "System wakeup disabled by ACPI" until PNP started up. Plug n' Play
ACPI doesn't take over but rather runs alongside PCI

The patch module noradeon I made is inside the initramfs which is the earliest
any module can load:

[    1.177149] Unpacking initramfs...

This is so much later than when tables are parsed and executed but loading the
initramfs doesn't mean noradeon is even loaded. That doesn't happen until much
later on:

[    2.130285] noradeon: Radeon patch applied. Card disabled.

During that time DRM kicked in and registered the Intel device (GFX0) and ATI
(PEGP) video devices. Intel is the primary device. Later on when userspace
there are 10 ACPI warnings: "SystemIO range [...] conflicts with OpRegion"
which I suspected was related to audio

Used acpi_backlight=vendor as a kernel parameter and acpi_call to turn off the
card along with noradeon to be sure it stayed off. This allows the stock Arch
kernel, which doesn't have a custom_method flag (noradeon doesn't need it)

Tried to avoid using acpi_call since it's a security risk to have 
/proc/acpi/call open. The module takes in input from procfs and parses it to 
calculate how many arguments there are. Arguments are defined as being 
seperated by a space, so the input I pass in has 0 arguments. It then calls
do_acpi_call(method, nargs, args) which for this system is:

do_acpi_call('\\_SB.PCI0.P0P2.PEGP._OFF', 0, NULL)

Internally this method calls acpi_evaluate_object(handle, NULL, &arg, &buffer)
but &arg is made of arg.count (which is 0) and arg.pointer (is NULL). ACPI 
source code allows the third and fourth arguments can be NULL if there's no 
parameters passed in. The buffer is not useful for this either

http://lxr.free-electrons.com/source/drivers/acpi/acpica/nsxfeval.c#L175

Result:
status = acpi_evaluate_object('\\_SB.PCI0.POP2.PEGP._OFF', NULL, NULL, NULL)

Allows everything to work with little issue
