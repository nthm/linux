Leaf - HP Envy 14

No longer being used. Was a daily driver from 2010 to early 2019. There are no
dotfiles in this repo, only notes on btrfs superblock corruption and ATI
graphics card issues. Wrote a DKMS module to disable the card.
