# Nectarine

This is the successor to Lychee. It replaces the huge multi-drive desktop with
my old laptop (see _systems/laptop-leaf-arch_). Quiet this way.

All the services from Lychee were migrated, but this repo will only have new
services, or changes, instead of copy/pasting everything all at once.

## Running

- Arch with BTRFS
- Host: Postgres and Git
- Containers: Traefik(+LE), Bitwarden, Nextcloud, Gotify, Wordpress*, Portainer,
  Watchtower, Docker Registry, and Portainer/Echo/Whoami.
