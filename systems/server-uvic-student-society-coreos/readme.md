# University student society hosting provider

This is a CoreOS install that autoupdates and runs only Docker containers.

It hosts containers for services and websites of other clubs within the
building. Sharing is caring.

The infrastructure will be similar to my homeserver that uses Traefik and
Portainer as central themes for isolating services. This is done in Traefik 2.0
and requires a lot of unfortunate path prefix magic since subdomains are not
supported by DNS on the campus network.

The goal is a close to zero-maintenance system for the student society. The
ignition config sets up two RAID mirrored drives, but otherwise all maintenance
is done through the clubs themselves by using the web UI for container
management. RBAC in Portainer handles the security. No one should ever SSH in.

It's nice beacuse Traefik does TCP now so MySQL and SSH etc can be subject to
the same middleware and logging.

## Issues with Traefik V2

People have a lot of mixed feelings about Traefik 2 and a lot say it's not worth
the verbose configuration. I feel that.

Take HTTPS redirection. Used to be 1 line and is now 6 lines:

https://docs.traefik.io/migration/v1-to-v2/#http-to-https-redirection-is-now-configured-on-routers

Similarly, path rewriting went from 1 to 3 lines - everytime you want to do it.

https://docs.traefik.io/migration/v1-to-v2/#strip-and-rewrite-path-prefixes
