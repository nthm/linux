## Hosting

When hosting applications, it used a _jwilder/nginx-proxy_ container as the
reverse proxy of the host machine. It automated the subdomain creation of
containers with an exposed web port, while still providing options to customize
everything you could need in an Nginx config.

Paired with Portainer and a file manager like _filebrowser/filebrowser_ people
didn't need to SSH into a box for any kind of configuration.
