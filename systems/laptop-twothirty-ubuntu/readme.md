TwoThirty - Lenovo ThinkPad X230T

Ubuntu 20.04 Focal w/ LUKS. Cozy. Still using Parabola/Arch on homeservers.
Replaced X61T. Using Podman to host local infrastructure instead of Docker.

For Podman, it's rootless, so then the kernel needs to be tuned to allow ports
less than 1024 since their networking doesn't work with `setcap` and
CAP_NET_BIND_SERVICE. The ".gitkeep" files are for bind mounts. Use mkcert to
get localhost certificates for Traefik.

Using Traefik to mirror the homeserver infrastructure of Traefik + Branchpoint +
Ory.sh/Kratos/Hydra.
