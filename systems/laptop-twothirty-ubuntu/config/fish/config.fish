#!/usr/bin/fish

# Disable welcome
set fish_greeting

# Ubuntu doesn't sbin for getcap/setcap
set -x PATH /usr/bin /usr/sbin

# Using `n` instead, but:
# set NPM_PACKAGES "$HOME/_/npm"
# set PATH $NPM_PACKAGES/bin $PATH
# set MANPATH $NPM_PACKAGES/share/man $MANPATH

set -x N_PREFIX "$HOME/_/n"
set -x PATH $N_PREFIX/bin $PATH

# Personal scripts
set -x PATH $HOME/_/bin $PATH

set -x VISUAL nano
set -x EDITOR nano

alias systemctl 'sudo systemctl'
alias mount 'sudo mount'
alias umount 'sudo umount'

function _runsudo --description 'Add sudo'
    set -l cursor_pos (echo (commandline -C) + 5 | bc)
    commandline -C 0
    commandline -i 'sudo '
    commandline -C "$cursor_pos"
end
# "Ctrl-Root"? Ctrl-S is taken because Fish uses it for searching in menus
bind \cr "_runsudo"

function !! --description "Retry with sudo"
    set -l prev_command $history[1]
    if test $status -eq 0
        echo "Exited 0. Not retrying: $prev_command"
        return 1
    end
    echo "sudo $prev_command"
    sudo (string split ' ' $prev_command)
end

functions --query realcd; or functions --copy cd realcd
function cd
    if test "$argv" = "-"
        echo "Hop: $PWD"
    end
    # Using `builtin` would skip Fish's cd function that supports "-"
    realcd $argv
    test $status -eq 0; and ll
end

function ls
    command ls -XFh --group-directories-first --human-readable --sort=extension --color=always $argv
end
